function [ model ] = getModel( data, labels, tagset)
%GETMODEL Summary of this function goes here
%   Detailed explanation goes here
    gestures = gestureCuts(data,labels);
    dist = gestures(:,3)-gestures(:,2);
    mm = mean(dist);
    [~, idx] = sort(abs(dist - mm));
    model = data(gestures(idx(1),2):gestures(idx(1),3), :);
end

