%%%%%%%%%%% Human Behavior Analysis %%%%%%%%%%
clear;
clc;
close all;

addpath('./dtw');

%% Load data
[data, labels, tagset] = load_file('P1_1_9A_p21');
% Extract the start and end frame of each gesture from the sequence.
gestures = gestureCuts(data,labels);

%% Sequence gesture comparison with autovalidation
D = zeros(size(gestures,1), size(gesturesB,1));
for i=1:size(gestures,1)
    for j=1:size(gesturesB,1)
        x1 = data(gestures(i,2):gestures(i,3), :);
        x2 = data(gesturesB(j,2):gesturesB(j,3), :);
        
        tic;
        D(i,j) = DTWDistance(x1,x2);
        t=toc;
        fprintf('Comparing %d with %d\n',i,j);
        fprintf('  Distance\t%f\n  Elp. time\t%f\n',D(i,j),t);  
    end
end

%% display matrix
Dd= D/max(D(:)); % normalization


%D = D./125;
h = imagesc(Dd);
axis square
caxis([0 1])
colormap(jet(100))
colorbar

%%
x1 = data(gestures(9,2):gestures(9,3), :);
x2 = data(gesturesB(10,2):gesturesB(10,3), :);
[Dist, D]= DTWDistance(x1,x2);
[path,k] = computePath(D);


%%
[db, lb, tb] = load_file('P1_1_9A_p19');
[da, la, ta] = load_file('P2_2_5A_p30');
AA = gestureCuts(da,la);
BB = gestureCuts(db,lb);
[ eq, D ] = computeSimilarSubsequences(db, lb, da, la, 10);


%%

tag         = 4;
model       = 'P2_2_1_p26';
Z = one_vs_all_class(model,tag);