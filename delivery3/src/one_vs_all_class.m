

function [Z]= one_vs_all_class(model, tag)

dd = dir('../data');

[da, la, ta] = load_file(model);
A = gestureCuts(da,la);
Z = zeros(size(dd,1),1);


for i=1:size(dd,1);
    
    fn = getfield(dd(i),'name');
    
    if (strcmp(fn, '.') || strcmp(fn, '..')); 
        continue;
    end
    fn = strsplit(fn,'.');
    filename = char(fn(1));
    sp = strsplit(filename,'_');
    
    if ( size(sp,2) == 4 ...
            && size(strfind(char(sp(3)),num2str( tag )),1) ~= 0 ...
            && strcmp(regexp(char(sp(3)),'\d+', 'match'), num2str(tag)) ...
            && ~strcmp(filename, model) ...
            )
        
        [db, lb, tb] = load_file(filename);
        B = gestureCuts(db,lb);
        [ eq, D ] = computeSimilarSubsequences( da, la, db, lb, 0 );
        Z(i) = min(min(D));
        
    end
    
end