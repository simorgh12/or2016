[G1, model, trainingNum] = getTrainingModel(0.2, 1);


%%
exp = 'P\d_\d_\dA?_p\d\d?';
dd = dir('../data');
counter = 0;

%%
% generate training set
for i=1:size(dd,1)
    filename = char(regexp(char(dd(i).name), exp, 'match'));
    if (size(filename,1) ~= 0)
       [data, labels, tagset] = load_file(filename);
       gestures = gestureCuts(data,labels);
       
       for j=1:size(gestures,1);
           counter = counter+1;
           target = data(gestures(j,2):gestures(j,3), :);
           [Dist,D] = DTWDistance(G1,target);

     
           Z(:,~isnan(D(1,:))) = Z(:,~isnan(D(1,:)))+D;
           mc(:,~isnan(D(1,:))) = mc(:,~isnan(D(1,:)))+1;
           if (counter == trainend)
              broken = 1;
              break; 
           end
       end
       
       if (exist('broken', 'var'))
           break;
       end
    end
end