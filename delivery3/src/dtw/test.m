clear;
clc;
close all;

%% generate data
a = rand(500,3);
b = rand(500,3);

%% dynamic programming
tic;
[Dist,D,path] = dtw(a,b);
t=toc;
fprintf('Distance\t%f\nElpased time\t%f\n',Dist,t);

%% display accumulated distance
D = D/max(abs(D(:)));    % normalization
h = imagesc(D);
axis square
caxis([0 1])
colormap(gray(100))
colorbar
hold on
plot(path(:,1),path(:,2),'red');