function [ eq, D ] = computeSimilarSubsequences( dX, lX, dY, lY, th )
%INPUT
%   X: train gestures
%   Y: test gestures
%   th: Threshold

X = gestureCuts(dX,lX);
Y = gestureCuts(dY,lY);

D = zeros(size(X,1), size(Y,1));
for i=1:size(X,1)
    for j=1:size(Y,1)
        x1 = dX(X(i,2):X(i,3), :);
        x2 = dY(Y(j,2):Y(j,3), :);
        D(i,j) = DTWDistance(x1,x2);
    end
end

eq = (min(min(D)) < th);

end

