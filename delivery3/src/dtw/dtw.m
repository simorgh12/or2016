%% Dynamic Time Warping v.2
function [Dist,D,path,k]=dtw(t,r)
    % Dynamic Time Warping Algorithm
    % Dist is unnormalized distance between t and r
    % D is the accumulated distance matrix
    % k is the normalizing factor
    % path is the optimal path
    % t is the vector you are testing against
    % r is the vector you are testing
    N=size(t,1);
    M=size(r,1);
    
    %% cost
    for n=1:N
        for m=1:M
            % Euclidean distance
            d(n,m)=norm(t(n,:)-r(m,:));
        end
    end
    
    %% initialization
    D=zeros(size(d));
    D(1,1)=d(1,1);
    for n=2:N
        D(n,1) = d(n,1) + D(n-1,1);
    end
    for m=2:M
        D(1,m) = d(1,m) + D(1,m-1);
    end
    
    %% begin dynamic programming
    for n=2:N
        for m=2:M
            D(n,m)=d(n,m)+min([D(n-1,m),D(n-1,m-1),D(n,m-1)]);
        end
    end
    Dist=D(N,M);
    
    %% path reconstruction
    n=N; m=M;
    k=1;
    path=[];
    path(1,:)=[N,M];
    while ((n+m)~=2)
        if (n-1)==0
            m=m-1;
        elseif (m-1)==0
            n=n-1;
        else 
            [~,number]=min([D(n-1,m),D(n,m-1),D(n-1,m-1)]);
            switch number
                case 1
                    n=n-1;
                case 2
                    m=m-1;
                case 3  % diagonal
                    n=n-1;
                    m=m-1;
            end
        end
        k=k+1;
        path=cat(1,path,[n,m]);
    end
    
end

