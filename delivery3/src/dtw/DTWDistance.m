%% Dynamic Time Warping Algorithm
function [Dist,D] = DTWDistance(s,t)
    % s is the vector you are testing against
    % t is the vector you are testing
    %
    % Dist is unnormalized distance between s and t
    % D is the accumulated distance matrix
    
    %% check arguments
    if size(s,2)~=size(t,2)
        error('Dimensions of the input signals do not match.');
    end
    
    %% initialization
    n=size(s,1);
    m=size(t,1);
    D = zeros(n+1,m+1)+Inf; % cache matrix
    D(1,1) = 0;

    %% begin dynamic programming
    for i=1:n
        for j=1:m
            cost = norm(s(i,:)-t(j,:)); % Euclidean distance
            D(i+1,j+1) = cost+min([D(i,j+1), D(i+1,j), D(i,j)]); % insertion, deletion, match
        end
    end
    Dist=D(n+1,m+1);
    D=D(2:end,2:end); % skip 1st row/col (Inf)
end