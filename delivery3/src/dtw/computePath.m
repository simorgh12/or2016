%% Optimal path reconstruction
function [path,P,k] = computePath(D)
    % D is the accumulated distance matrix
    %
    % path is the optimal path
    % k is the normalizing factor
    
    [N,M]=size(D);
    n=N; m=M;
    k=1;
    path=[];
    path(1,:)=[N,M];
    while ((n+m)~=2)
        if (n-1)==0
            m=m-1;
        elseif (m-1)==0
            n=n-1;
        else 
            [~,number]=min([D(n-1,m),D(n,m-1),D(n-1,m-1)]);
            switch number
                case 1
                    n=n-1;
                case 2
                    m=m-1;
                case 3  % diagonal
                    n=n-1;
                    m=m-1;
            end
        end
        k=k+1;
        path=cat(1,path,[n,m]);
        
    end
    path=flipud(path);
    P=zeros(size(path,1),1);
    
    for i=1:size(path,1)
        P(i) = D(path(i,1),path(i,2));
    end
  
end
