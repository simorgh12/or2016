function [G1] = getTrainingModel(perctrain, tag)

exp = strrep('P\d_\d_=numberA?_p\d\d?', '=number', sprintf('%d', tag));
dd = dir('../data');
counter = 0;

for i=1:size(dd,1);
    filename = char(regexp(char(dd(i).name), exp, 'match'));
    if (size(filename,1) ~= 0)
       [data, labels, tagset]=load_file(filename);
       gestures = gestureCuts(data,labels);
       counter = counter+size(gestures,1);
    end
end

trainstart  = 0;
trainend    = floor(counter*perctrain);
teststart   = floor(counter*perctrain)+1;
testend     = counter;


counter = 0;
%%
% generate training set
for i=1:size(dd,1)
    filename = char(regexp(char(dd(i).name), exp, 'match'));
    if (size(filename,1) ~= 0)
       [data, labels, tagset] = load_file(filename);
       gestures = gestureCuts(data,labels);
       if (~exist('model', 'var'));
           model = getModel(data, labels, tagset);
           Z = zeros(size(model,1),500);
           mc = zeros(size(model,1),500);
       end
       
       for j=1:size(gestures,1);
           counter = counter+1;
           target = data(gestures(j,2):gestures(j,3), :);
           [Dist,D] = DTWDistance(model,target);

     
           Z(:,~isnan(D(1,:))) = Z(:,~isnan(D(1,:)))+D;
           mc(:,~isnan(D(1,:))) = mc(:,~isnan(D(1,:)))+1;
           if (counter == trainend)
              broken = 1;
              break; 
           end
       end
       
       if (exist('broken', 'var'))
           break;
       end
    end
end
%%
Z = Z./mc;
G1 = Z(:,~isnan(mean(Z,1)));
% now, we will place a training



% files(i).name = filename;
end
