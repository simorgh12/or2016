function T = forward_solve(I,H)
    % FORWARD_SOLVE
    % Perform the FORWARD projection, scanning all pixels x and compute
    % x' = H x
    
    T = zeros(size(I));
    for i=1:size(I,1)
        for j=1:size(I,2)
            x = [j;i;1];
            xi = H*x;
            xn =xi/xi(3,1);
            if(xn(1,1)>=0 && xn(2,1)>=0 && xn(3,1)>=0)
                T(round(1+xn(2,1)),round(1+xn(1,1)))=I(i,j);
            end
        end
    end
    T=uint8(T);
end