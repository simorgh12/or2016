function T = inverse_transform(I,H)
    % INVERSE_TRANSFORM
    % Perform the inverse_transformation projection, scanning all pixels x and compute
    % x = inv(H) x'
    
    T = zeros(size(I));
    for i=1:size(I,1)
        for j=1:size(I,2)
            x = [j;i;1];
            xi = H\x;
            xn = xi/xi(3,1);     %% NORM
            xx = xn(2,1); yy=xn(1,1);
            
            % the mapping is out of the scope, put the value to 0 and
            % continue
            if(xx < 1 || xx > size(I,1) || yy < 1 || yy > size(I,2))
                continue
            end
            
            % Now let's interpolate the pixel using bilinear interpolation
            T(i,j) = bilinear(I,xx,yy);
        end
    end
    T=uint8(T);
end