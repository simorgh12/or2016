function pxx = bilinear(Ia, xx, yy)
% BILINEAR interpolation
    x1=floor(xx);x2=ceil(xx);
    y1=floor(yy);y2=ceil(yy);

    q11 = Ia(x1,y1)*(x2-xx)*(y2-yy);
    q21 = Ia(x2,y1)*(xx-x1)*(y2-yy);
    q12 = Ia(x1,y2)*(x2-xx)*(yy-y1);
    q22 = Ia(x2,y2)*(xx-x1)*(yy-y1);
    pxx = (1./((x2-x1)*(y2-y1)))*(q11+q12+q21+q22);          
end

