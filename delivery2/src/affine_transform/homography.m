function H = homography( fa, fb, matches )
%OBTAIN_COORDS Summary of this function goes here
%   Detailed explanation goes here

    pin     = zeros(size(matches));
    pout    = zeros(size(matches));
    for i=1:size(matches,2)
        x  = matches(1, i);
        xt = matches(2, i);

        x = fa(1:2,x);
        xt = fb(1:2,xt);

        pin(1,i) = x(1);
        pin(2,i) = x(2);

        pout(1,i) = xt(1);
        pout(2,i) = xt(2);

    end
    H = homography_solve(pin, pout);
end

