%%%%%%%%%%% Automatic Correspondence %%%%%%%%%%
run('../util/vlfeat-0.9.16/toolbox/vl_setup');
addpath('./img_manipulation');
addpath('./affine_transform');



%% DEFINE THE PARAMETERS
%
im1= '../LabImages/set1/llibre1.jpg';
im2= '../LabImages/set1/llibre2.jpg';
num_keypoints = 6;
threshold = 4.;



%%
% Let's start by reading the images from the 

Ia = imread(im1);
Ib = imread(im2);
% ..And prepare them to the automatic keypoint detection by setting them to
% greyscale
Iaa = single(rgb2gray(Ia));
Ibb = single(rgb2gray(Ib));

%%
% Obtain sift keypoints, fa vector will contain the center, size and
% orientation of the keypoints and da are the feature vectors of the
% keypoints

[fa, da] = vl_sift(Iaa);
[fb, db] = vl_sift(Ibb);

%%
% Match keypoints from original and transformed images
[matches, scores] = vl_ubcmatch(da, db, threshold);
[scores,I] = sort(scores, 'descend');
% Select the best 8 matches
matches_t = matches(:,I(1:num_keypoints));
figure,
show_matches(Iaa,Ibb,fa,fb,matches_t);

%%
% Calculate the homography matrix
H = homography(fa,fb, matches_t);

%% Apply affine homography to a given Image
Ict = forward_solve(rgb2gray(Ia), H);
figure,
subplot(1,3,1)
imshow(Ia)
subplot(1,3,2)
imshow(Ib)
subplot(1,3,3)
imshow(Ict)
%%
Ic = inverse_transform(rgb2gray(Ia),H);
figure,
subplot(1,3,1)
imshow(Ia)
subplot(1,3,2)
imshow(Ib)
subplot(1,3,3)
imshow(Ic)

%%


figure,
imshow(Ia)
figure,
imshow(Ib)
figure,
imshow(Ic)
figure,
imshow(Ict)