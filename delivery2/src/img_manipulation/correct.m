function Z = correct(I, ran)
    off = floor(ran/2);
    Z = zeros(size(I));
    for i=1+off:size(I,1)-off
        for j=1+off:size(I,2)-off
            if (I(i,j) == 0)
                xf = i-off; xl = i+off;
                yf = j-off; yl = j+off;
                
                mask = I(xf:xl,yf:yl);
                count = sum(sum(mask>0));
                mn = sum(sum(mask));
                mn = mn/double(count);
                if ~isnan(mn)
                    Z(i,j) = mn;
                end
            else
                Z(i,j) = I(i,j);
            end
        end
    end
    Z = uint8(Z);
end