%%%%%%%%%%% Manual Correspondence %%%%%%%%%%
addpath('./img_manipulation');
addpath('./affine_transform');

clc;
close all;
clear all;

% In order to completely specify the matrix H, only 4 correspondences are
% needed (which gives rise to 8 equations).
Ia = rgb2gray(imread('../LabImages/set1/llibre1.jpg'));
Ib = rgb2gray(imread('../LabImages/set1/llibre2.jpg'));
nPoints = 4;    % Number of matches to select

%% Manual selection (uncomment section to set new points)
% imshow(Ia);
% [x,y] = ginput(nPoints);
% fa = [x y]
% 
% imshow(Ib);
% [x,y] = ginput(nPoints);
% fb = [x y]

%% Manually selected matches
pin  = [[185 193 567 543]; [116 434 151 468]];  %[x1 y1; x2 y2; ...]
pout = [[127 144 563 553]; [132 466 131 327]];

%% Show manual matches
figure,
imagesc(cat(2, Ia, Ib));
colormap('gray');
hold on; % Prevent image from being blown away.
for n=1:nPoints
    xa = pin(1,n);
    xb = pout(1,n) + size(Ia,2);
    ya = pin(2,n);
    yb = pout(2,n);
    
    plot(xa, ya, 'go');
    plot(xb, yb, 'go');
    h = line([xa xb], [ya yb]) ;
    set(h,'linewidth', 1, 'color', 'g');
end
hold off;
truesize;
axis equal; axis off; axis tight;

%% Compute homography matrix H
% Such an equation may be solved using standard techniques for solving
% linear equations. In this case we use the 'Singular value decomposition'  
% (SVD) method.
H = homography_solve(pin, pout);
% y = homography_transform(pin, H); % Apply homography to some given points

%% Apply affine homography to a given Image
Icf = forward_solve(Ia,H);
Ict = inverse_transform(Ia,H);

% figure,
% subplot(1,2,1)
% imshow(Icf);
% subplot(1,2,2)
% imshow(Ict);

figure,
imshow(Ic1);
figure,
imshow(Ic2);